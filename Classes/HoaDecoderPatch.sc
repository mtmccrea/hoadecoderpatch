HoaDecoderPatch {
	var decoderConfigPath, foclConfigPath, server;
	var <decoder, <multiBandFocl, <hoaDelays;
	var useKernels, useDelays;
	var <spDels, <spDists;
	var <decodersynth;
	classvar <exampleConfigPath;

	*initClass{
		exampleConfigPath = File.realpath(HoaDecoderPatch.filenameSymbol.asString.dirname +/+ "../config/")
	}

	*new { |decoderConfigPath, foclConfigPath, server|
		^super.newCopyArgs(decoderConfigPath, foclConfigPath, server).init
	}

	init {
		useDelays = false;
		useKernels = false;

		server = server ?? { Server.default };

		decoder = HoaDecoder.new(decoderConfigPath, server);

		foclConfigPath.notNil.if({
			useKernels = true;
			multiBandFocl = HoaMultiBandFocl.new(foclConfigPath, server)
		});

		spDists = decoder.spDists;

		// check if we have different distanced speakers, if not no need to run the delays

		spDists.do{ |del|
			(del != spDists[0]).if({
				useDelays = true;
			})
		};

		useDelays.if({
			spDels = spDists * 343.0.reciprocal;
			hoaDelays = HoaDelays.new(decoder.numSatChans, decoder.numSubChans, spDels)
		});

	}

	loadKernels {
		useKernels.if({
			multiBandFocl.kernelBuffers.do{ |buffer| buffer.load }
		})
	}

	addKernelsTo { |score|
		useKernels.if({
			multiBandFocl.kernelBuffers.do{ |buffer| buffer.addTo(score) }
		})
	}

	ar { |in, xover = (decoder.xover_freq), satGain = 0, subGain = 0|
		var sats, satIn, subIn;

		useKernels.if({
			in = multiBandFocl.ar(in);
			(decoder.numSubChans > 0).if({
				satIn = in[0..decoder.numBChannels-1];
				subIn = in[decoder.numBChannels..]
			}, {
				satIn = in
			})
		}, {
			satIn = in;
			subIn = in[decoder.subOrder.asHoaOrder.indices]
		});

		sats = decoder.ar(satIn, subIn, xover, satGain, subGain);

		useDelays.if({
			sats = hoaDelays.ar(sats)
		});

		^sats

	}



	synthDef { |name = 'HoaDecoderPatch'|
		^CtkSynthDef( name, {
			arg out_busnum=0, in_busnum, xover_freq = (decoder.xover_freq), fadeTime=0.2, satGain = 0, subGain=0, gate=1;
			var in, subIn, env, sat_out, sub_out;

			env = EnvGen.kr(Env([0, 1, 0], [fadeTime, fadeTime], 'sin', 1), gate);

			in = In.ar(in_busnum, decoder.numBChannels); // Hoa BFormat

			sat_out = this.ar(in, xover_freq, satGain, subGain) * env;

			// outs

			(decoder.outbusNums).do({ | spkdex, i |
				Out.ar(
					out_busnum + spkdex, // remap decoder channels to rig channels
					sat_out[i]
				)
			});


		})
	}

	stringToPathName { |path, relativePath = (PathName(File.realpath(HoaDecoder.filenameSymbol)).pathOnly)|
		PathName(path).isAbsolutePath.if({//it's absolute path or standard path
			^PathName.new(path);
		}, { // its relative path
			^PathName.new(relativePath ++ path)
		})

	}

	//convenience methods
	designModeMatcher { |beamShape = 'basic', match = 'energy'|
		decoder.designModeMatcher(beamShape, match)
	}

	designADTAllRad { |filename = 'ADT', beamShape = 'energy', match = 'amp', imagSpeakers, loadCondition|
		decoder.designADTAllRad(filename, beamShape, match, imagSpeakers, loadCondition)
	}

	designFromMatrix { |filename = 'ADT', beamShape = 'energy', match = 'amp', pathToMatrix|
		decoder.designFromMatrix(filename, beamShape, match, pathToMatrix)
	}

	designSubModeMatcher { |beamShape = 'basic', match = 'energy'|
		decoder.designSubModeMatcher(beamShape, match)
	}

	play { |inbus, outbus, xover, satGain, subGain, xfade = 0.2, addAction = 0, target = 1|
		decodersynth = this.synthDef.note(addAction: addAction, target: target)
		.in_busnum_(inbus)
		.out_busnum_(outbus);

		xover !? { decodersynth.xover_freq_(xover) };
		satGain !? { decodersynth.satGain_(satGain) };
		subGain !? { decodersynth.subGain_(subGain) };

		decodersynth.fadeTime_(xfade).play;
	}

	pause {
		decodersynth.pause;
	}

	unPause {
		decodersynth.run;
	}

	free { |xfade|
		xfade !? { decodersynth.fadeTime_(xfade) };
		decodersynth.release;
	}
}