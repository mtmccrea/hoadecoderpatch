HoaDelays {
	var numSatChans, numSubChans, delays;

	*new { |numSatChans, numSubChans, delays|
		^super.newCopyArgs(numSatChans, numSubChans, delays)
	}

	ar { |in|
		var maxDelay;

		maxDelay = delays.maxItem;

		^DelayN.ar( in, maxDelay, delays );

	}

}
