HoaMultiBandFocl {
	var configPath, server;
	var <config, <kernelBuffers;
	var <alpha, <kernelPath, <psyFoclKernelPath, <psyFoclSize, <foclRadius, <mainBeamDict;
	var <mainDim, <match, <satOrder, <sampleRate, <subBeamDict, <subDim, <subOrder;
	var numBChannels, numSubBChannels;
	classvar <exampleConfigPath;

	*initClass{
		exampleConfigPath = File.realpath(HoaDecoder.filenameSymbol.asString.dirname +/+ "../config/")
	}

	*new { |configPath, server|
		^super.newCopyArgs(configPath, server).init
	}

	init {

		config = configPath.notNil.if({
			this.stringToPathName(configPath).fullPath.load
		}, {
			IdentityDictionary.new(know: true) // make empty
		});

		server = server ?? { Server.default };

		this.loadAttributes;

		this.prGenerateFoclKernels;

		numBChannels = satOrder.asHoaOrder.size;
		numSubBChannels = subOrder.asHoaOrder.size;

		psyFoclKernelPath.deepFiles.do{ |pathname|
			(pathname.extension == "wav").if({
				kernelBuffers = kernelBuffers.add(
					CtkBuffer(pathname.fullPath, server: server)
				)
			})
		}
	}

	synthDef {

		CtkSynthDef('soundLabHoaConv', { |in_busnum, out_busnum, gate = 1, fadeTime|
			var in, sats, subs, env;

			env = EnvGen.kr(Env([0, 1, 0], [fadeTime, fadeTime], 'sin', 1), gate);

			in = In.ar(in_busnum, numBChannels);

			Out.ar(out_busnum, this.ar(in) * env)

		});

	}

	ar { |in|
		var sats, subs;
		// sats
		sats = kernelBuffers[0..satOrder].collect{ |buffer, i|
			Convolution2.ar(
				in[i.asHoaDegree.indices],
				buffer,
				framesize: psyFoclSize
			)
		}.flatten;

		subOrder.notNil.if({
			sats = sats ++ kernelBuffers[satOrder+1..].collect{ |buffer, i|
				Convolution2.ar(
					in[i.asHoaDegree.indices],
					buffer,
					framesize: psyFoclSize
				)
			}.flatten;
		});

		^sats
	}

	loadAttributes {
		var atts;
		satOrder = config.satOrder ?? { 3 };
		subOrder = config.subOrder; // default to nil for no subs

		sampleRate = config.sampleRate ?? { server.sampleRate ?? { 48000 } };

		// unpack dec specs
		kernelPath = this.stringToPathName(config.psyFoclKernelPath ?? { Platform.defaultTempDir });

		psyFoclSize = config.psyFoclSize ?? { 1024 };
		alpha = config.alpha ?? { 1.0 };  // kaiser smoothing window
		foclRadius = config.foclRadius ?? { AtkHoa.refRadius * 0.5 };   // focalisation radius
		mainBeamDict = config.mainBeamDict ?? {
			IdentityDictionary(know: true)
			.putPairs([
				\beamShapes, [ \energy, \controlled ],
				\edgeFreqs, [ 3000.0, 10000.0 ],
			])
		};
		mainDim = config.satDim ?? { 3 };
		match = config.match ?? { 'rms' };
		subOrder.notNil.if({
			subBeamDict = config.subBeamDict ?? {
				IdentityDictionary(know: true)
				.putPairs([
					\beamShapes, [ \energy ]
				])
			};
			subDim = config.subDim ?? { 2 }
		});

		psyFoclKernelPath = kernelPath.fullPath ++ sampleRate.asInteger ++ "/";

		atts = [satOrder, psyFoclSize, alpha, foclRadius, mainBeamDict.beamShapes, mainBeamDict.edgeFreqs, mainDim, match];

		subOrder.notNil.if({
			atts = atts ++ [subBeamDict.beamShapes, subDim];
		});

		atts.do{ |param|
			psyFoclKernelPath = psyFoclKernelPath ++ param ++ "_"
		};

		psyFoclKernelPath = psyFoclKernelPath[0..psyFoclKernelPath.size-2] ++ "/";

		psyFoclKernelPath = PathName(psyFoclKernelPath.replace("[", "").replace("]", "").replace(" ", "").replace(",", "_"));

	}

	prGenerateFoclKernels {

		psyFoclKernelPath.isFolder.not.if({
			var mainPsychoKernels, subPsychoKernels, kernels;
			psyFoclKernelPath.fullPath.postln;
			psyFoclKernelPath.fullPath.mkdir;

			// design kernels
			mainPsychoKernels = Signal.hoaMultiBandFocl(
				size: psyFoclSize,
				radius: foclRadius,
				beamDict: mainBeamDict,
				dim: mainDim,
				match: match,
				order: satOrder,
				sampleRate: sampleRate
			) * HoaOrder.new(satOrder).beamWeights(\energy, mainDim).reciprocal;

			mainPsychoKernels = mainPsychoKernels.collect({ |kernel|
				Signal.kaiserWindow(psyFoclSize, a: alpha) * kernel // window
			});

			kernels = mainPsychoKernels;

			subOrder.notNil.if({
				subPsychoKernels = Signal.hoaMultiBandFocl(
					size: psyFoclSize,
					radius: foclRadius,
					beamDict: subBeamDict,
					dim: subDim,
					match: match,
					order: subOrder,
					sampleRate: sampleRate
				);

				subPsychoKernels = subPsychoKernels.collect({ |kernel| Signal.kaiserWindow(psyFoclSize, a: alpha) * kernel });  // window

				kernels = kernels ++ subPsychoKernels
			});



			kernels.do({ |kernel, i|
				kernel.write(
					path: psyFoclKernelPath.fullPath ++ "FIR_" ++ i.asString.padLeft(2, "0") ++ ".wav",
					headerFormat: "WAV",
					sampleFormat: "float",
					sampleRate: sampleRate
				)
			})  // as a sound file

		})
	}

	stringToPathName { |path, relativePath = (PathName(File.realpath(HoaDecoder.filenameSymbol)).pathOnly)|
		PathName(path).isAbsolutePath.if({//it's absolute path or standard path
			^PathName.new(path);
		}, { // its relative path
			^PathName.new(relativePath ++ path)
		})

	}
}