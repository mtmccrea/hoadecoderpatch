HoaDecoder {
	var configPath, server;
	var config;
	var <satOrder, <subOrder;
	var <decoderSynth;
	var <numBChannels, <numSubBChannels, <numSubChans, <numSatChans;
	var <outbusNums, <satDirections, <subDirections;
	var <>matrix_dec_sat, <>matrix_dec_sub;
	var <xover_freq;
	var <spDists, <spGains;
	classvar <exampleConfigPath;

	*initClass{
		exampleConfigPath = File.realpath(HoaDecoder.filenameSymbol.asString.dirname +/+ "../config/")
	}

	*new { |configPath|
		^super.newCopyArgs(configPath).init;
	}

	init {

		config = this.stringToPathName(configPath).fullPath.load;

		numSatChans = config.numSatChans { 8 };
		numSubChans = config.numSubChans ?? { 0 };

		outbusNums = config.outbusNums ?? { (0..numSatChans + numSubChans-1) };

		satOrder = config.satOrder ?? { 3 };
		subOrder = config.subOrder ?? { 1 };

		satDirections = config.directions[0..numSatChans - 1] ?? { numSatChans.collect{ |inc| inc/numSatChans * 2pi } };
		subDirections = config.directions[numSatChans..] ?? { numSubChans.collect{ |inc| inc/numSatChans * 2pi } }; // could be empty array with sub size of zero check if this works

		xover_freq = config.xover_freq; // default to nil, no crossover (maybe crossover in hardware?)

		(satDirections.size != numSatChans).if({ Error("Given number of satellites %, does not equal given number of directions".format(numSatChans, satDirections.size)).throw });
		(subDirections.size != numSubChans).if({ Error("Given number of subs %, does not equal given number of directions".format(numSubChans, subDirections.size)).throw });

		numBChannels = HoaOrder.new(satOrder).size;
		numSubBChannels = HoaOrder.new(subOrder).size;

		spDists = config.spkrDists ?? { (numSatChans + numSubChans).collect{ AtkHoa.refRadius } };

		spGains = config.spkrGains ?? { (numSatChans + numSubChans).collect{ 0.0 } };

	}

	designModeMatcher { |beamShape = 'basic', match = 'energy'|
		matrix_dec_sat = HoaMatrixDecoder.newModeMatch(satDirections, beamShape, match, satOrder)
	}

	designADTAllRad { |filename = 'ADT', beamShape = 'energy', match = 'amp', imagSpeakers, loadCondition|
		{
			var adt, cond = Condition.new;
			// will write to ~/Library/Application Support/ATK/extensions/matrices/HOA(satOrder)/decoders/
			adt = ADT.new(
				directions: satDirections,
				match: match,
				order: satOrder,
				filename: filename
			);
			adt.allrad(imagSpeakers, cond);
			cond.wait;
			this.designFromMatrix(filename, beamShape, match);
			loadCondition !? { loadCondition.test_(true).signal }
		}.forkIfNeeded
	}

	designFromMatrix { |filename = 'ADT', beamShape = 'energy', match = 'amp', pathToMatrix|
		pathToMatrix.notNil.if({
			matrix_dec_sat = HoaMatrixDecoder.newFromFile(pathToMatrix, false, satOrder)
		}, {
			matrix_dec_sat = HoaMatrixDecoder.newFromFile(filename ++ "-allrad-beam-" ++ beamShape ++ "-match-" ++ match ++ ".yml", order: satOrder);
		});
	}

	designSubModeMatcher { |beamShape = 'basic', match = 'energy'|
		matrix_dec_sub = HoaMatrixDecoder.newModeMatch(subDirections, beamShape, match, subOrder)
	}

	synthDef { |synthDefName = 'HoaDecoder'|

		^CtkSynthDef( synthDefName, {
			|out_busnum=0, in_busnum, sub_in_busnum, xover_freq = 80, fadeTime=0.2, satGain = 0, subGain=0, gate=1|
			var in, subIn, env, out, sub_out;

			env = EnvGen.kr(Env([0, 1, 0], [fadeTime, fadeTime], 'sin', 1), gate);

			in = In.ar(in_busnum, numBChannels); // Hoa Sats

			subIn = In.ar(sub_in_busnum, numSubBChannels); // Hoa1 Subs

			out = this.ar(in, subIn, xover_freq, satGain) * env;

			// outs
			outbusNums.do({ | spkdex, i |
				Out.ar(
					out_busnum + spkdex, // remap decoder channels to rig channels
					out[i]
				)
			})
		})

	}

	ar { |in, subIn, xover = (this.xover_freq), satGain = 0, subGain = 0|
		var sats, subs;
		// near-field compensate, decode, remap to rig
		sats = matrix_dec_sat.matrix.rows.collect({ | matrixRow|
			(
				HoaNFCtrl.ar(
					in,
					AtkHoa.refRadius,
					spDists.at(matrixRow).abs,
					satOrder
				) * matrix_dec_sat.matrix.getRow(matrixRow)
			).sum
		});

		// apply xover if necessary
		xover_freq.notNil.if({
			sats = HPF.ar(HPF.ar(sats, xover), xover)
		});

		(numSubChans > 0).if({
			/* -- sub decode --*/
			subs = matrix_dec_sub.notNil.if({
				matrix_dec_sub.matrix.rows.collect({ | matrixRow|
					(
						HoaNFCtrl.ar(
							subIn,
							AtkHoa.refRadius,
							spDists.at(numSatChans + matrixRow).abs,
							subOrder
						) * matrix_dec_sub.matrix.getRow(matrixRow)
					).sum
				})
			}, {
				numSubChans.collect({
					in[0] * 2.sqrt // send W scaled by 3db
				})
			});

			// apply xover if necessary
			xover_freq.notNil.if({
				subs = LPF.ar(LPF.ar(subs, xover), xover)
			});

			^(sats * spGains[0..numSatChans-1].dbamp * satGain.dbamp) ++ (subs * spGains[numSatChans..].dbamp * subGain.dbamp)
		}, {
			sats * spGains[0..numSatChans-1].dbamp * satGain.dbamp
		})
	}

	stringToPathName { |path, relativePath = (PathName(File.realpath(HoaDecoder.filenameSymbol)).pathOnly)|
		PathName(path).isAbsolutePath.if({//it's absolute path or standard path
			^PathName.new(path);
		}, { // its relative path
			^PathName.new(relativePath ++ path)
		})

	}

}


